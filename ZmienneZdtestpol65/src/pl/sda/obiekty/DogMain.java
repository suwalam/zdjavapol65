package pl.sda.obiekty;

public class DogMain {

    public static void main(String[] args) {

        Dog reksio = new Dog("Owczarek niemiecki", "Reksio", 11.5);

        Dog misiek = new Dog("kundel", "Misiek", 5.6);

        System.out.println("Pies o nazwie " + reksio.getName() + " jest rasy "
                + reksio.getBreed() + " waży " + reksio.getWeight() + " kg i szczeka tak:");
        reksio.bark();

        System.out.println("Pies o nazwie " + misiek.getName() + " jest rasy "
                + misiek.getBreed() + " waży " + misiek.getWeight() + " kg i szczeka tak:");
        misiek.bark();


        reksio.setWeight(9.6);
        reksio.bark();

        reksio.setWeight(-10);
        System.out.println("Waga reksia po aktualizacji: " + reksio.getWeight());

        reksio.setName(null);
        System.out.println("Imie reksia po aktualizacji: " + reksio.getName());

    }

}
