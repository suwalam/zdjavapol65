package pl.sda.tablice;

public class ArrayExamples {

    public static void main(String[] args) {

        int[] array = new int[]{ 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };

        System.out.println(array[0]);
        System.out.println(array[9]);

        array[0] = 11;
        System.out.println(array[0]);
        System.out.println("Długość tablicy array: " + array.length);

        int array2[] = new int[10];
        System.out.println(array2[0]);
        System.out.println(array2[9]);

        for (int i=0; i< array.length; i++) {
            //array[i] = array[i] * 2;
            //ten zapis jest równy powyższemu
            array[i] *= 3;
            System.out.println(array[i]);
        }

        //wypisz liczby w podziale na parzyste i nieparzyste z tablicy array
        for (int i=0; i< array.length; i++) {

            if (array[i] % 2 == 0) {
                System.out.printf("liczba %d jest parzysta\n", array[i]);
            } else {
                System.out.printf("liczba %d jest nieparzysta\n", array[i]);
            }
        }

        //nie można odwołać się do nieistniejącej komórki w tablicy
        //System.out.println(array[10]);

        //pętla for each służy tylko do odczytu elementów tablicy
        for (int element : array) {
            element = element * 100;
            System.out.print(element + " "); //element to tylko kopia oryginalnej wartości z tablicy
        }

        for (int element : array) {
            System.out.print(element + " ");
        }

    }

}
