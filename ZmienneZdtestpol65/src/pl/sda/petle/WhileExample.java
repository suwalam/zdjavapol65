package pl.sda.petle;

public class WhileExample {

    //ctr + alt + L = formatowanie kodu

    public static void main(String[] args) throws InterruptedException {
        boolean myFlag = true;
        int iter = 0;
        while (myFlag) {
            System.out.print(iter++);
            if (iter == 5) {
                myFlag = false;
            }

            System.out.println("x");

            Thread.sleep(2000); //2 sekundy uśpienia

        }

    }

}
