package pl.sda.petle;

import java.util.Random;
import java.util.Scanner;

public class DoWhileExample {

    public static void main(String[] args) {


//        int iter = 0;
        int iter = 6;
        do {
            System.out.print(iter++);
            System.out.print("x");
        } while (iter < 5);


        //***************************************

        Random r = new Random();

        int random = r.nextInt(11); //losuje liczby z przedziału 0-10

        Scanner in = new Scanner(System.in);

        int input;


        do {
            System.out.println("Podaj liczbę: ");
            input = in.nextInt();

        } while (random != input);

        System.out.println("Brawo zgadłeś");

    }

}
