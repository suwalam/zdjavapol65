package pl.sda.petle;

public class BreakAndContinueExample {

    public static void main(String[] args) {

        for (int i = 0; i < 8; ++i) {
            System.out.print(i);

            if (i == 3) {
                continue; //przerwany zostaje bieżący obrót pętli
            }

            if (i == 5) {
                break; //przerwane zostaje działanie całej pętli
            }

            System.out.print("x");
        }
    }

}
