package pl.sda.petle;

public class ForExample {

    public static void main(String[] args) {

        for (int iter=0; iter < 5; iter++) {
            System.out.println("Iter wynosi: " + iter);
        }

        //**********************************
        int iter2 = 0;
        while (iter2 < 5) {
            System.out.println("Iter 2 wynosi: " + iter2);
            iter2++;
        }

        //**********************************
        int iter3 = 0;
        do {
            System.out.println("Iter 3 wynosi: " + iter3);
            iter3++;
        } while (iter3 < 5);

    }

}
