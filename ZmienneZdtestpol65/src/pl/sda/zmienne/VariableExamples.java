package pl.sda.zmienne;

public class VariableExamples {



    public static void main(String[] args) {

        //Wypisuje na ekran napis i dodaje na końcu znak nowej linii

        // ctrl + / - komentarz
        //ctrl + Shift + / - /**/
        //sout - System.out.println

        System.out.println("Hello World!");
        System.out.println("Hello World 2!");

        int intVar = -10;
        System.out.println("Zawartość zmiennej intVar " + intVar);


        intVar = 1000;
        System.out.println("Zawartość zmiennej intVar po aktualizacji " + intVar);

        boolean boolVar = true;

        byte byteVar = 120; //zakres od -128 - 127
        byteVar = (byte) 260;
        System.out.println("Zawartość zmiennej byteVar po aktualizacji " + byteVar);

        //-128, -127, -126, ..... 126, 127


        long longVar = 1000000000165785785L;

        float floatVar = 10.99999F;

        double doubleVar= 11.54353463422;

        char charVar = 'a';
        charVar = '@';

        //znak \ jest znakiem specjalnym
        System.out.println("\n"); //enter, przejście do nowej linii
        charVar = '\''; //apostrof to znak specjalny i musi byc poprzedzony backslashem

        System.out.println("Zawartość zmiennej charVar po aktualizacji " + charVar);

        charVar = '9';
        charVar = 90;
        System.out.println("Zawartość zmiennej charVar po aktualizacji " + charVar);

        longVar = byteVar;
        intVar = byteVar;
        doubleVar = longVar;
        longVar = (long) floatVar;
        System.out.println("Zawartość zmiennej longVar po aktualizacji " + longVar);


        Integer integerVar = Integer.valueOf(100);
        int sum = integerVar + 20;

        integerVar = null;

        String myVar = new String("This String is String!");
        int beginIndex = 2;
        String otherVar = "This";
        System.out.println(myVar.substring(beginIndex)); // is String is String!

        System.out.println("Is string empty? " + "".isEmpty());

    }



}
