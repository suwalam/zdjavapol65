package pl.sda.zmienne;


import java.util.InputMismatchException;
import java.util.Scanner;


public class InputOutputExamples {

    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);

        System.out.println("Podaj swoje imię: ");

        String name = in.nextLine();

        System.out.println("Witaj, " + name + ". Podaj swój wiek: ");

        short age;

        try {
            age = in.nextShort(); //niebezpieczeństwo podania przez użytkownika ciągu znaków
        } catch (InputMismatchException e) {
            age = -1;
            in = new Scanner(System.in); //reset skanera do ustawień pierwotnych
        }

        if (age >= 0 && age < 18) {
            System.out.println("Jesteś niepełnoletni");
        } else if (age >= 18) {
            System.out.println("Jesteś pełnoletni");
        } else {
            System.out.println("Podałeś nieprawidłową wartość");
        }

        System.out.println("Podaj miesiąc urodzenia: ");
        byte month = in.nextByte();

        String text = "Urodziłeś/aś się w ";

        switch (month) { //zmienna wyboru
            case 1:
                System.out.println(text + "styczniu"); break;
            case 2:
                System.out.println(text + "lutym"); break;
            case 3:
                System.out.println(text + "marcu"); break;
            case 4:
                System.out.println(text + "kwietniu"); break;
            case 5:
                System.out.println(text + "maju"); break;

            default:
                System.out.println("Nieobsługiwana wartość!");  break;
        }

        System.out.println("Koniec programu");

    }

}
